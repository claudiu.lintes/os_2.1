#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>


#define SIZE 4096
/*
S1

Description: The proposed project combines functionalities for monitoring a directory to manage differences between two captures (snapshots) of it. The user will be able to observe and intervene in the changes in the monitored directory.
Directory Monitoring:
The user can specify the directory to be monitored as an argument in the command line, and the program will track changes occurring in it and its subdirectories, parsing recursively each entry from the directory.
With each run of the program, the snapshot of the directory will be updated, storing the metadata of each entry

(Parse directory and for each entry )
*/

int main(int argc,char* argv[]){
    struct stat buffer;
    struct dirent *directoryinfo;
    DIR* directory;
    char outbuffer[SIZE];
    if(argc!=2){
        perror("Bad number of arguments");
        exit(1);
    }
    
    stat(argv[1],&buffer);
    if(!S_ISDIR(buffer.st_mode)){
        perror("The argument is not a directory");
        exit(2);
    }
    int fd=open("output.txt", O_WRONLY | O_CREAT | O_TRUNC,S_IRUSR | S_IWUSR);
    if(fd==-1){
        perror("Error in opening the file");
        exit(3);
    }
    int outBufferLenght;
    directory=opendir(argv[1]);
    while((directoryinfo=readdir(directory))){
        stat(directoryinfo->d_name,&buffer);
        outBufferLenght=snprintf(outbuffer,SIZE,"%s | %lu | %d | %d | %ld | %ld | %ld",directoryinfo->d_name,buffer.st_dev,buffer.st_mode,buffer.st_uid,buffer.st_size,buffer.st_size,buffer.st_atime);
        if(outBufferLenght<0){
            perror("Error in snprint");
            exit(4);
        }
        write(fd,&outbuffer,strlen(outbuffer));
        write(fd,"\n",1);
    }
}