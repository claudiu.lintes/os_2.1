#!/bin/bash

num=$#
input_file=$1
output_folder=$2
if test ! $num -eq 2
then echo "bad number of arguments"; exit 1;
fi

if test ! -f $input_file
then echo "input is not a file";exit 2;
fi
chmod +r $input_file
while read word
do
if [["${word,,}" =~ ^(corrupted|dangerous|attack|malware|malicious)$]]
then move $input_file $output_file; echo File was isolated to the quarantine folder $output_folder
done
