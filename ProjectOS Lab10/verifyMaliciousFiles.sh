#!/bin/bash

numberOfArguments=$#
input_file=$1
if test ! $numberOfArguments -eq 1
then echo "bad number of arguments"; exit 1; #check if the number of arguments is good
fi

if test ! -f $input_file
then echo "input is not a file";exit 2; #check if the argument is a file
fi
chmod +r $input_file #give the permission of read to the file 
wcResult=$(wc $input_file) #read wc results 
numberOfLines=$(echo "$wcResult" | awk '{print $1}') #we extract the number of lines using awk
numberOfWords=$(echo "$wcResult" | awk '{print $2}') #we extract the number of words using awk
numberOfCharacters=$(echo "$wcResult" | awk '{print $3}') #we extract the number of characters using awk
if test "$numberOfLines" -lt 2 && test "$numberOfWords" -gt 1000 && test "$numberOfCharacters" -gt 2000 #we test the suspicious condition
then 

if LC_ALL=C grep -q '[^[:print:]]' "$input_file" #if a file is suspicious we check if we have have non ascii characters using grep
then echo $input_file; exit 3;
fi
if grep -q -iE 'corrupted|dangerous|attack|malware|malicious' "$input_file" #we also check if we have the specific words using grep
then echo $input_file; exit 4;
fi
fi
echo "SAFE" #we output safe if all the test pass , if not the name of the file will be outputed