#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>


#define SIZE 4096
#define FIRST_DIRECTORY 5           //Define the argument number where the directory start in the command

//Command Example: ./a.out -o output.txt -s QuarantineDirectory DIR1 DIR2 DIR1

/*
S1

Description: The proposed project combines functionalities for monitoring a directory to manage differences between two captures (snapshots) of it. The user will be able to observe and intervene in the changes in the monitored directory.
Directory Monitoring:
The user can specify the directory to be monitored as an argument in the command line, and the program will track changes occurring in it and its subdirectories, parsing recursively each entry from the directory.
With each run of the program, the snapshot of the directory will be updated, storing the metadata of each entry

(Parse directory and for each entry )

S2

-----This is the second part of the project----

The functionality of the program will be updated to allow it to receive an unspecified number of arguments (directories) in the command line. The logic for capturing metadata will now apply to all received arguments, meaning the program will update snapshots for all directories specified by the user.

For each entry of each directory provided as an argument, the user will be able to compare the previous snapshot of the specified directory with the current one. If there are differences between the two snapshots, the old snapshot will be updated with the new information from the current snapshot.

The functionality of the code will be expanded so that the program receives an additional argument, representing the output directory where all snapshots of entries from the specified directories in the command line will be stored. This output directory will be specified using the `-o` option. For example, the command to run the program will be: `./program_exe -o output input1 input2 ...`.

*/
int bannedArgumentPossitions[SIZE]={0}; //hold the banned position 
char outputFileName[SIZE/16];
char quarantineDirectoryName[SIZE/16];
char scriptName[]="verifyMaliciousFiles.sh"; //holds the script name for file verification
int compareStats(char DirectoryName1[],char DirectoryName2[]){ //return 0 if directories (with the same name) are equal, 1-6 otherwise depending on the first found difference 
    if(strcmp(DirectoryName1,DirectoryName2)!=0){
        return 0; //We return 0 if they are different directories
    }
    struct stat buffer1;
    struct stat buffer2;
    DIR* directory1;
    DIR* directory2;
    struct dirent *directoryinfo1;
    struct dirent *directoryinfo2;
    stat(DirectoryName1,&buffer1);
    stat(DirectoryName2,&buffer2);
    if(!S_ISDIR(buffer1.st_mode)){
        perror("compareStats: -The 1st Directory is not a directory");
        exit(2);
    }
    if(!S_ISDIR(buffer2.st_mode)){
        perror("compareStats: -The 2nd Directory is not a directory");
        exit(2);
    }
    directory1=opendir(DirectoryName1);
    directory2=opendir(DirectoryName2);
    while((directoryinfo1=readdir(directory1))||(directoryinfo2=readdir(directory2))){
        stat(directoryinfo1->d_name,&buffer1);
        stat(directoryinfo2->d_name,&buffer2);
        if(buffer1.st_dev!=buffer2.st_dev){
            return 1;
        }
        if(buffer1.st_mode!=buffer2.st_mode){
            return 2;
        }
            if(buffer1.st_uid!=buffer2.st_uid){
            return 3;
        }
            if(buffer1.st_size!=buffer2.st_size){
            return 4;
        }
            if(buffer1.st_atime!=buffer2.st_atime){
            return 5;
        }
    }
    if(readdir(directory1)!=NULL||readdir(directory2)!=NULL){
        return 6;
    }
    return 0;
}

void initializeFileNames(char* outputName,char* quarantineName){
    strcpy(outputFileName,outputName);
    struct stat buffer;
    stat(quarantineName,&buffer);
        if(!S_ISDIR(buffer.st_mode)){
        perror("initializeFileNames: -<quarantineName> is not a directory");
        exit(2);
    }
    strcpy(quarantineDirectoryName,quarantineName);
}

void checkCommandIntegity (char *arguments[],int numberOfArguments){
    if(numberOfArguments<=FIRST_DIRECTORY){
        perror("checkCommandIntegrity: -Wrong number of arguments");
        exit(1);
    }
    if(strcmp(arguments[1],"-o")!=0||strcmp(arguments[3],"-s")!=0){
        perror("checkCommandIntegrity: -Command is missing flags [-o <FILE> -s <DIRECTORY>]");
        exit(4);
    }
    initializeFileNames(arguments[2],arguments[4]);
}

int checkPermissionsAndAnalyze(struct stat currentFileStat,char* CurrentDirectoryName){
    char * arguments[]={scriptName,CurrentDirectoryName};
    int pid;
    int pfd[2];
    FILE *stream;
    char output[SIZE/16];
    char path[SIZE/8];
    if(!((currentFileStat.st_mode&&S_IRUSR)||(currentFileStat.st_mode&&S_IWUSR)||(currentFileStat.st_mode&&S_IXUSR))){
        if(pipe(pfd)<0)
        {
        perror("checkPermissionAndAnalyze: -Pipe creation error\n");
        exit(6);
        }
        if((pid=fork())<0)//We create the grand child process 
        {
            perror("checkPermissionAndAnalyze: -Child process creation failed\n");
            exit(1);
        }
        if(pid==0){
            close(pfd[0]);
            dup2(pfd[1],1);
            execvp("verifyMaliciousFiles.sh",arguments);
            perror("checkPermissionAndAnalyze: -Execvp error");
        }
        close(pfd[1]);
        stream=fdopen(pfd[0],"r");
        fscanf(stream,"%s",output);
        if(strcmp(output,"SAFE")!=0){
            snprintf(path,sizeof(path),"%s/%s",quarantineDirectoryName,CurrentDirectoryName);
            if(rename(CurrentDirectoryName,path)==-1){
                perror("checkPermissionAndAnalyze: -Rename error");
            }
            else{
                return 1;
            }
        }
    }
    return 0;
}

int outputDirectoryStats(char DirectoryName[],int fileFD){ //returns how many files were corrupted in the directory
    struct stat buffer;
    DIR* directory;
    struct dirent *directoryinfo;
    stat(DirectoryName,&buffer);
    char outbuffer[SIZE];
    int outBufferLenght;
    int result=0;
    if(!S_ISDIR(buffer.st_mode)){
        perror("outputDirectoryStats: -The Directory is not a directory");
        exit(2);
    }
    directory=opendir(DirectoryName);
    if(fileFD==-1){
        perror("Error in opening the file");
        exit(3);
    }
    write(fileFD,DirectoryName,strlen(DirectoryName));
    write(fileFD,"\n",1);
    while((directoryinfo=readdir(directory))){
        if(strcmp(directoryinfo->d_name,".")==0||strcmp(directoryinfo->d_name,"..")==0){
            if((directoryinfo=readdir(directory))==NULL){
                break;
            }
        }
        stat(directoryinfo->d_name,&buffer);
        result=checkPermissionsAndAnalyze(buffer,directoryinfo->d_name)+result; //count how many files are corrupted
        outBufferLenght=snprintf(outbuffer,SIZE,"%s | %lu | %d | %d | %ld | %ld",directoryinfo->d_name,buffer.st_dev,buffer.st_mode,buffer.st_uid,buffer.st_size,buffer.st_atime);
        if(outBufferLenght<0){
            perror("Error in snprint");
            exit(5);
        }
        write(fileFD,&outbuffer,strlen(outbuffer));
        write(fileFD,"\n",1);
    }
    return result;
}

void parseAndBan(char* arguments[],int numArguments){
    for(int i=FIRST_DIRECTORY;i<numArguments;i++){
        for(int j=i+1;j<numArguments;j++){
            if(compareStats(arguments[i],arguments[j])!=0&&bannedArgumentPossitions[i]!=1){
                bannedArgumentPossitions[i]=1; //we ban the old argument directory, ONLY if the new directory argument has any new changes
                printf("Directory %d has been banned\n",i-4);
            }
        }
    }
}

void createAndPrintOutput(char* argument[],int numArguments){ //creates and prints the process
    int pid;
    int resultBufferLenght;
    char firstMessage[]="-Parrent Process-\n\n";
    char resultBuffer[SIZE];
    int processIndex=1;
    int fileFD=open(outputFileName, O_WRONLY | O_CREAT | O_TRUNC,S_IRUSR | S_IWUSR);
    int status;
    int numberOfCorruptedFiles;
    int pfd[2];
    char pipeBuffer[SIZE/32];
    if(fileFD==-1){
        perror("Error in opening the file");
        exit(3);
    }
    if(pipe(pfd)<0){
        perror("createAndPrintOutput: -Pipe creation error\n");
        exit(6);
    }
    write(fileFD,firstMessage,strlen(firstMessage));
    for(int i=FIRST_DIRECTORY;i<numArguments;i++){
        if(bannedArgumentPossitions[i]==0){
            if((pid=fork())<0){
                perror("Error in creation of the child process | fork() ");
            }
            else if(pid==0){
                resultBufferLenght=snprintf(resultBuffer,SIZE,"Child Process [%d]\n",processIndex);
                if(resultBufferLenght<0){
                    perror("Error in snprint");
                    exit(5);
                }
                write(fileFD,&resultBuffer,strlen(resultBuffer));
                numberOfCorruptedFiles=outputDirectoryStats(argument[i],fileFD);
                snprintf(pipeBuffer,sizeof(pipeBuffer),"%d",numberOfCorruptedFiles);
                close(pfd[0]);
                write(pfd[1],pipeBuffer,sizeof(pipeBuffer));
                close(pfd[1]);
                exit(0);
            }
            if(wait(&status)>=0){;
                if (WIFEXITED(status))
                {
                    close(pfd[1]);
                    read(pfd[0],pipeBuffer,sizeof(pipeBuffer));
                    close(pfd[0]);
                    printf("Child process [%d] terminated with PID %d with %d status having %s files with potential danger\n",processIndex,pid,WEXITSTATUS(status),pipeBuffer);
                    processIndex++;
                }
            }
        }
    } //this function must also print the snapshots in the child process area!
}

/*
exit 0 good
exit 1 Wrong Number of arguments
exit 2 Not a directory
exit 3 Error in file opening
exit 4 Flags are missing
exit 5 snprint() error
exit 6 Pipe error
*/

int main(int argc,char* argv[]){
    checkCommandIntegity(argv,argc);
    parseAndBan(argv,argc);
    createAndPrintOutput(argv,argc);
}