#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>


#define SIZE 4096
/*
S1

Description: The proposed project combines functionalities for monitoring a directory to manage differences between two captures (snapshots) of it. The user will be able to observe and intervene in the changes in the monitored directory.
Directory Monitoring:
The user can specify the directory to be monitored as an argument in the command line, and the program will track changes occurring in it and its subdirectories, parsing recursively each entry from the directory.
With each run of the program, the snapshot of the directory will be updated, storing the metadata of each entry

(Parse directory and for each entry )

S2

-----This is the second part of the project----

The functionality of the program will be updated to allow it to receive an unspecified number of arguments (directories) in the command line. The logic for capturing metadata will now apply to all received arguments, meaning the program will update snapshots for all directories specified by the user.

For each entry of each directory provided as an argument, the user will be able to compare the previous snapshot of the specified directory with the current one. If there are differences between the two snapshots, the old snapshot will be updated with the new information from the current snapshot.

The functionality of the code will be expanded so that the program receives an additional argument, representing the output directory where all snapshots of entries from the specified directories in the command line will be stored. This output directory will be specified using the `-o` option. For example, the command to run the program will be: `./program_exe -o output input1 input2 ...`.

*/
char DirectoryNames[SIZE/4][SIZE];

int containsDirectory(char string[SIZE],int lenght){ //lenght will represent the last index added
    for(int i=0;i<=lenght;i++) {
        if(strcmp(string,DirectoryNames[i])==0){
            //strcpy(DirectoryNames[i],"\0");
            return 1;
        }
    }
    return 0;
}

void emptyDirectoryNames(int lenght){
    int i=0;
    for(i=0;i<=lenght;i++){
        strcpy(DirectoryNames[i],"\0");
    }
}

int main(int argc,char* argv[]){
    struct stat buffer;
    struct dirent *directoryinfo;
    DIR* directory;
    char outbuffer[SIZE];
    /*if(argc!=2){
        perror("Bad number of arguments");
        exit(1);
    }*/
    
    stat(argv[1],&buffer);
    if(!S_ISDIR(buffer.st_mode)){
        perror("The argument is not a directory");
        exit(2);
    }
    int bannedArgumentPossitions[SIZE]={0};
    int outBufferLenght;
    int numberOfArguments=argc;
    int directoryNamesIndex=0; 
    int i;
        int fd=open("output.txt", O_WRONLY | O_CREAT | O_TRUNC,S_IRUSR | S_IWUSR);
    if(fd==-1){
        perror("Error in opening the file");
        exit(3);
    }
    for (i=1;i<numberOfArguments;i++){
        while(bannedArgumentPossitions[i]==1){ //we skip the banned arguments
            printf("\n%d is banned",i);
            i++;
        }
        if(containsDirectory(argv[i],directoryNamesIndex)==1){
            printf("\n%d is contained\n",i);
            for (int j=1;j<numberOfArguments;j++){
                if((strcmp(argv[i],argv[j])==0)&&bannedArgumentPossitions[j]!=1){
                    printf("We ban %d\n",j);
                    bannedArgumentPossitions[j]=1; //we ban the argument 
                    break;
                }
            }
            emptyDirectoryNames(directoryNamesIndex);
            directoryNamesIndex=0;
                fd=open("output.txt", O_WRONLY | O_CREAT | O_TRUNC,S_IRUSR | S_IWUSR);
                printf("Delete\n");
                if(fd==-1){
                    perror("Error in opening the file");
                    exit(3);
                }
            i=0; //we parse the arguments again skiping the banned possition
        }
        else{
            strcpy(DirectoryNames[directoryNamesIndex],argv[i]);
            directoryNamesIndex++;
        }
        if(i!=0){
            printf("\nWrite %d ",i);
            write(fd,argv[i],strlen(argv[i]));
            directory=opendir(argv[i]);
            write(fd,"\n",1);
            while((directoryinfo=readdir(directory))){
                stat(directoryinfo->d_name,&buffer);
                outBufferLenght=snprintf(outbuffer,SIZE,"%s | %lu | %d | %d | %ld | %ld",directoryinfo->d_name,buffer.st_dev,buffer.st_mode,buffer.st_uid,buffer.st_size,buffer.st_atime);
                if(outBufferLenght<0){
                    perror("Error in snprint");
                    exit(4);
                }
                write(fd,&outbuffer,strlen(outbuffer));
                write(fd,"\n",1);
            }
        }
    }
}
