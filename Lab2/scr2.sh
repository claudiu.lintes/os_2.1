#!/bin/bash
if test -z "$1"  || test -z "$2" || test -z "$3" || test ! -z "$4" 
then echo "The number of operators is incorrect"
elif test "$2" = "+"
then echo $(($1+$3))
elif test "$2" = "-"
then echo $(($1-$3))
elif test "$2" = "p"
then echo $(($1*$3))
elif test "$2" = "/"
then echo $(($1/$3))
else echo "The operator isn't correct"
fi