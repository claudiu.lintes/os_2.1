#!/bin/bash
user=$(whoami)
hour=$(date +%H)
if test "$hour" -lt 12 && test $hour -ge 6
then echo "Good Morning $user"
elif test "$hour" -lt 18 && test $hour -ge 12
then echo "Good Afternoon $user"
elif test "$hour" -lt 22 && test $hour -ge 18
then echo "Good Evening $user"
else echo "Good Night $user"
fi